package INF101.lab2;

import INF101.lab2.pokemon.IPokemon;
import INF101.lab2.pokemon.Pokemon;

public class Main {

    public static IPokemon pokemon1;

    public static IPokemon pokemon2;

    public static void main(String[] args) {
        pokemon1 = new Pokemon("Pikachu");
        pokemon2 = new Pokemon("Charmander");

        System.out.println("Start of the battle between " + ((Pokemon) pokemon1).getName() + " and "
                + ((Pokemon) pokemon2).getName());

        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            pokemon1.attack(pokemon2);
            if (pokemon2.isAlive()) {
                pokemon2.attack(pokemon1);
            }
        }

        if (pokemon1.isAlive()) {
            System.out.println(((Pokemon) pokemon1).getName() + " is the winner!");
        } else {
            System.out.println(((Pokemon) pokemon2).getName() + " is the winner!");
        }
    }
}
